package com.cc.scratch.simple.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author danledgard
 */
 
@Controller
public class SimpleController {
 
    @RequestMapping("home")
    public String loadHomePage(Model m) {
        m.addAttribute("name", "Dan");
        return "home";
    }
}
